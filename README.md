This project has the following functional:
```
The task is to develop an web UI application to search for github repos. The requirements are as follows:

- Search form with filters, such as:
    * bulk search(by name or part of name)
    * programming language
    * number of stars (range)
    * licence
- Pretty frontend part (preloaders, IE 8 compatable)
- Any technology stack
- README with usage

If all filters filled, use a combination of these filters.
In case all filters are empty display the top 10 repos.
```

## How install
1. `yarn install`
2. `yarn start`

## How start unit tests
2. `yarn test`
