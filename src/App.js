import React from 'react';
import { Provider } from 'react-redux';

import HomePage from './page/Home';
import store from './reducers';

const Application = () => (
    <Provider store={store}>
        <HomePage />
    </Provider>
);

export default Application;
