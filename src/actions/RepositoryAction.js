//@flow
import { Action } from '../models/Actions';
import * as RepositoryModel from '../models/RepositoryModel';

export const PERFORM_CHANGE_REPOSITORIES_LIST = 'PERFORM_CHANGE_REPOSITORIES_LIST';
export const PERFORM_PENDING_ENABLE_REPOSITORIES_LIST = 'PERFORM_PENDING_ENABLE_REPOSITORIES_LIST';
export const PERFORM_PENDING_DISABLE_REPOSITORIES_LIST = 'PERFORM_PENDING_DISABLE_REPOSITORIES_LIST';
export const PERFORM_CHANGE_CONDITION_SEARCH = 'PERFORM_CHANGE_CONDITION_SEARCH';

export type PERFORM_CHANGE_CONDITION_SEARCH_TYPE = {
    value: any,
    typeField: RepositoryModel.TypeCondition,
};
export const performChangeConditionSearch = (
    value: any,
    typeField: RepositoryModel.TypeCondition,
): Action<PERFORM_CHANGE_CONDITION_SEARCH_TYPE> => ({
    type: PERFORM_CHANGE_CONDITION_SEARCH,
    payload: {
        value,
        typeField,
    },
});

export type PERFORM_CHANGE_REPOSITORIES_LIST_TYPE = RepositoryModel.RepositoriesState;
export const performChangeRepository = (
    value: RepositoryModel.RepositoriesState,
): Action<PERFORM_CHANGE_REPOSITORIES_LIST_TYPE> => ({
    type: PERFORM_CHANGE_REPOSITORIES_LIST,
    payload: value
});


export type PERFORM_PENDING_ENABLE_REPOSITORIES_LIST_TYPE = boolean;
export const performEnablePendingRepository = (): Action<PERFORM_PENDING_ENABLE_REPOSITORIES_LIST_TYPE> => ({
    type: PERFORM_PENDING_ENABLE_REPOSITORIES_LIST,
    payload: true
});

export type PERFORM_PENDING_DISABLE_REPOSITORIES_LIST_TYPE = boolean;
export const performDisablePendingRepository = (): Action<PERFORM_PENDING_DISABLE_REPOSITORIES_LIST_TYPE> => ({
    type: PERFORM_PENDING_DISABLE_REPOSITORIES_LIST,
    payload: false
});
