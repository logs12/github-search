import React from 'react';
import injectSheet from 'react-jss'
import { LinearProgress } from 'rmwc/LinearProgress';
import { Chip, ChipText } from 'rmwc/Chip';

const styles = {
    repositoryContainer: {
        maxWidth: '500px',
        margin: '20px auto',
        repositoryContainerItem: {
            padding: '10px 0',
            repositoryUrl: {
                cursor: 'pointer',
            },
            '& .mdc-chip': {
                height: '20px',
            }
        }
    }
}


export const RepositoryListComponent = injectSheet(styles)(({
    classes,
    repositoryList,
    isPending
}) => {
    return (
        <div className={classes.repositoryContainer}>
            { isPending ?
                <LinearProgress determinate={false}></LinearProgress>
                : repositoryList.map((repository, index) => (
                    <div
                        key={index}
                        className={classes.repositoryContainerItem}
                    >
                        <h3>
                            <a
                                className={classes.repositoryUrl}
                                href={repository.html_url}
                            >
                                { repository.full_name }
                            </a>
                        </h3>
                        {
                            repository.language &&
                            <div>
                                language: <Chip><ChipText>{ repository.language }</ChipText></Chip>
                            </div>
                        }
                        {
                            repository.license &&
                                <div>
                                    license: <Chip><ChipText>{ repository.license.name }</ChipText></Chip>
                                </div>
                        }

                        <hr />
                    </div>
                ))
            }
        </div>
    );
});