// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import type { Connector } from 'react-redux';

import { RepositoryListComponent } from './component';

import * as RepositoryModel from '../../models/RepositoryModel';
import * as RepositoryThunk from '../../thunks/RepositoryThunk';

export interface StateProps {
    repositoryList: RepositoryModel.RepositoriesState,
    isPending: RepositoryModel.RepositoriesState,
}

export interface DispatchProps {
    performSubmitSearch: Function;
    performGetDefaultRepositoryList: Function;
    handleChangeSearchInput: Function;
    handleClickButttonSearch: Function;
}

export type RepositoryListProps = StateProps & DispatchProps;

class RepositoryList extends Component<RepositoryListProps, StateProps> {

    componentDidMount() {
        this.props.performGetDefaultRepositoryList();
    }

    render() {

        const {
            repositoryList,
            isPending,
        } = this.props;

        return (
            <RepositoryListComponent
                repositoryList={repositoryList}
                isPending={isPending}
            />
        )
    }
}

const mapStateToProps = (state) => ({
    repositoryList: state.repositories.data.items,
    isPending: state.repositories.isPending,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    performSubmitSearch: (conditions: RepositoryModel.SearchConditionsRepositoryState) => {
        dispatch(RepositoryThunk.performSubmitSearch(conditions));
    },
    performGetDefaultRepositoryList: () => {
        dispatch(RepositoryThunk.performGetDefaultRepositoryList());
    },
});

const connector: Connector<StateProps, DispatchProps> = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default connector(RepositoryList);
