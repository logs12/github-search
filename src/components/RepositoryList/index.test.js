import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import RepositoryList from './index';
import * as RepositoryModel from '../../models/RepositoryModel';

describe('>>> RepositoryList--- Shallow Render REACT COMPONENTS', () => {
    
    const initialState = { repositories: RepositoryModel.initialState.state };
    const mockStore = configureStore();
    let store, container;
    
    
    beforeEach(() => {
        store = mockStore(initialState);
        container = shallow(
            <RepositoryList
                store={store}
            />
        )
    });

    it('+++ render the connected(SMART) component', () => {
        expect(container.length).toEqual(1);
    });

    it('+++ check Prop matches with initialState', () => {
        expect(container.prop('repositoryList')).toEqual(initialState.repositories.data.items);
        expect(container.prop('isPending')).toEqual(initialState.repositories.isPending);
    });
});