import React from 'react';
import injectSheet from 'react-jss'
import { Button } from 'rmwc/Button';
import { Select } from 'rmwc/Select';
import { TextField } from 'rmwc/TextField';
import { Slider } from 'rmwc/Slider';
import { Grid, GridCell } from 'rmwc/Grid';

const styles = {
    searchComponent: {
        maxWidth: '500px',
        margin: '0 auto',
        searchComponentFieldSearchContainer: {
            '& .mdc-text-field': {
                width: '100%',
            }
        },
        searchComponentFieldFilterContainer: {
            fieldFilterItem: {
                padding: '0 10px',
                width: '100%',
            },
            '& .mdc-select': {
                width: '100%',
            }
        }
    }
}

const LanguagesList = [
    {
        label: 'JavaScript',
        value: 'JavaScript'
    },
    {
        label: 'Java',
        value: 'Java'
    },
    {
        label: 'Python',
        value: 'Python'
    },
    {
        label: 'Objective-C',
        value: 'Objective-C'
    },
    {
        label: 'Go',
        value: 'Go'
    },
    {
        label: 'Ruby',
        value: 'Ruby'
    },
    {
        label: 'PHP',
        value: 'PHP'
    },
    {
        label: 'C++',
        value: 'C++'
    },
    {
        label: 'C',
        value: 'C'
    },
    {
        label: 'Swift',
        value: 'Swift'
    },
]

const LicenceList = [
    {
        label: 'Academic Free License v3.0',
        value: 'afl-3.0'
    },
    {
        label: 'Apache license 2.0',
        value: 'apache-2.0'
    },
    {
        label: 'Artistic license 2.0',
        value: 'artistic-2.0'
    },
    {
        label: 'Boost Software License 1.0',
        value: 'bs1-1.0'
    },
];

export const SearchComponent = injectSheet(styles)(({
    classes,
    handleChangeSearchField,
    handleClickButttonSearch,
    searchConditions,
}) => {
    return (
        <div className={classes.searchComponent}>
            <div className={classes.searchComponentFieldSearchContainer}>
                <TextField
                    withLeadingIcon="search"
                    label="Search..."
                    fullwidth={false}
                    value={searchConditions.querySearch || ''}
                    onChange={event => handleChangeSearchField(event.target.value, 'querySearch')}
                />

            </div>
            <div className={classes.searchComponentFieldFilterContainer}>
                <Grid>
                    <GridCell span="6">
                        <Select
                            defaultValue={searchConditions.language || ''}
                            onChange={event => handleChangeSearchField(event.target.value, 'language')}
                            label="Languages"
                            placeholder="-- Select Languages --"
                            options={LanguagesList}
                        />
                    </GridCell>
                    <GridCell span="6">
                        <Select
                            defaultValue={searchConditions.licence || ''}
                            onChange={event => handleChangeSearchField(event.target.value, 'licence')}
                            label="Licence"
                            placeholder="-- Select Licence --"
                            options={LicenceList}
                        />
                    </GridCell>
                </Grid>
                <Grid>
                    <GridCell span="2">
                        <label>Stars:</label>
                    </GridCell>

                    <GridCell span="10">
                        <Slider
                            value={searchConditions.stars}
                            onChange={event => handleChangeSearchField(event.detail.value, 'stars')}
                            onInput={event => handleChangeSearchField(event.detail.value, 'stars')}
                            discrete
                            step={1}
                        />
                    </GridCell>
                </Grid>
            </div>
            <div className={classes.searchComponentButtonContainer}>
                <Button
                    raised
                    onClick={handleClickButttonSearch}
                >
                    Search
                </Button>
            </div>
        </div>
    );
});
