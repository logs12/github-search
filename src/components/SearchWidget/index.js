// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import type { Connector } from 'react-redux';

import { SearchComponent } from './component';

import * as RepositoryModel from '../../models/RepositoryModel';
import * as RepositoryThunk from '../../thunks/RepositoryThunk';

export interface StateProps {
    searchConditions: RepositoryModel.SearchConditionsRepositoryState,
}

export interface DispatchProps {
    performSubmitSearch: Function;
    handleChangeSearchInput: Function;
    handleClickButttonSearch: Function;
    performChangeConditionSearch: Function;
}

export type SearchWidgetProps = StateProps & DispatchProps;

class SearchWidget extends Component<SearchWidgetProps, StateProps> {

    componentWillReceiveProps(nextProps: SearchWidgetProps) {
        this.setState({ searchConditions: nextProps.searchConditions });
    }

    handleChangeSearchField = (value: any, typeField: RepositoryModel.TypeCondition) => {
        if (this.props.searchConditions[typeField] !== value) {
            this.props.performChangeConditionSearch(value, typeField);
        }
    }

    handleClickButttonSearch = () => {
        this.props.performSubmitSearch(this.state.searchConditions);
    }

    render() {
        const { searchConditions } = this.props;
        return (
            <SearchComponent
                searchConditions={searchConditions}
                handleChangeSearchField={this.handleChangeSearchField}
                handleClickButttonSearch={this.handleClickButttonSearch}
            />
        )
    }
}

const mapStateToProps = (state) => ({
    searchConditions: state.repositories.searchConditions,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    performSubmitSearch: (conditions: RepositoryModel.SearchConditionsRepositoryState) => {
        dispatch(RepositoryThunk.performSubmitSearch(conditions));
    },
    performChangeConditionSearch: (value: any, typeField: RepositoryModel.TypeCondition) => {
        dispatch(RepositoryThunk.performChangeConditionSearch(value, typeField));
    },
});

const connector: Connector<StateProps, DispatchProps> = connect(
    mapStateToProps,
    mapDispatchToProps
);

export default connector(SearchWidget);
