import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import SearchWidget from './index';
import * as RepositoryModel from '../../models/RepositoryModel';

describe('>>> SearchWidget--- Shallow Render REACT COMPONENTS', () => {
    
    const initialState = { repositories: RepositoryModel.initialState.state };
    const mockStore = configureStore();
    let store, container;
    
    
    beforeEach(() => {
        store = mockStore(initialState);
        container = shallow(
            <SearchWidget
                store={store}
            />
        )
    });

    it('+++ render the connected(SMART) component', () => {
        expect(container.length).toEqual(1);
    });

    it('+++ check Prop matches with initialState', () => {
        expect(container.prop('searchConditions')).toEqual(initialState.repositories.searchConditions);
    });
});