// @flow

export type LanguagesSearchConditions = Array<string>;

export type OwnerState = {
    "login": string | null,
    "id": number | null,
    "node_id": string | null,
    "avatar_url": string | null,
    "gravatar_id": string | null,
    "url": string | null,
    "html_url": string | null,
    "followers_url": string | null,
    "following_url": string | null,
    "gists_url": string | null,
    "starred_url": string | null,
    "subscriptions_url": string | null,
    "organizations_url": string | null,
    "repos_url": string | null,
    "events_url": string | null,
    "received_events_url": string | null,
    "type": string | null,
    "site_admin": false
};

export type LicenseState = {
    "key": string | null,
    "name": string | null,
    "spdx_id": string | null,
    "url": string | null,
    "node_id": string | null,
};

export type RepositoryItemState = {
    "id": number | null,
    "node_id": string | null,
    "name": string | null,
    "full_name": string | null,
    "owner": OwnerState,
    "private": boolean,
    "html_url": string | null,
    "description": string | null,
    "fork": boolean,
    "url": string | null,
    "forks_url": string | null,
    "keys_url": string | null,
    "collaborators_url": string | null,
    "teams_url": string | null,
    "hooks_url": string | null,
    "issue_events_url": string | null,
    "events_url": string | null,
    "assignees_url": string | null,
    "branches_url": string | null,
    "tags_url": string | null,
    "blobs_url": string | null,
    "git_tags_url": string | null,
    "git_refs_url": string | null,
    "trees_url": string | null,
    "statuses_url": string | null,
    "languages_url": string | null,
    "stargazers_url": string | null,
    "contributors_url": string | null,
    "subscribers_url": string | null,
    "subscription_url": string | null,
    "commits_url": string | null,
    "git_commits_url": string | null,
    "comments_url": string | null,
    "issue_comment_url": string | null,
    "contents_url": string | null,
    "compare_url": string | null,
    "merges_url": string | null,
    "archive_url": string | null,
    "downloads_url": string | null,
    "issues_url": string | null,
    "pulls_url": string | null,
    "milestones_url": string | null,
    "notifications_url": string | null,
    "labels_url": string | null,
    "releases_url": string | null,
    "deployments_url": string | null,
    "created_at": string | null,
    "updated_at": string | null,
    "pushed_at": string | null,
    "git_url": string | null,
    "ssh_url": string | null,
    "clone_url": string | null,
    "svn_url": string | null,
    "homepage": string | null,
    "size": number | null,
    "stargazers_count": number | null,
    "watchers_count": number | null,
    "language": string | null,
    "has_issues": boolean,
    "has_projects": boolean,
    "has_downloads": boolean,
    "has_wiki": boolean,
    "has_pages": boolean,
    "forks_count": number | null,
    "mirror_url": string | null,
    "archived": boolean,
    "open_issues_count": number | null,
    "license": LicenseState,
    "forks": number | null,
    "open_issues": number | null,
    "watchers": number | null,
    "default_branch": string | null,
    "score": number | null,
};

export type DataRepositoryState = {
    "total_count": number,
    "incomplete_results": boolean,
    "items": Array<RepositoryItemState>,
};

export type TypeCondition = "querySearch" | "stars" | "language" | "licence";

export type SearchConditionsRepositoryState = {
    querySearch: string | null,
    stars: number | null,
    language: string | null,
    licence: string | null,
};

export type RepositoriesState = {
    data: DataRepositoryState,
    isPending: boolean,
    searchConditions: SearchConditionsRepositoryState,
};

const defaultOwnerRepositoryState = {
    get state(): OwnerState {
        return {
            "login": null,
            "id": null,
            "node_id": null,
            "avatar_url": null,
            "gravatar_id": null,
            "url": null,
            "html_url": null,
            "followers_url": null,
            "following_url": null,
            "gists_url": null,
            "starred_url": null,
            "subscriptions_url": null,
            "organizations_url": null,
            "repos_url": null,
            "events_url": null,
            "received_events_url": null,
            "type": null,
            "site_admin": false
        }
    }
}

const defaultLicenseRepositoryState = {
    get state(): LicenseState {
        return {
            "key": null,
            "name": null,
            "spdx_id": null,
            "url": null,
            "node_id": null,
        }
    }
}

const defaultRepositoryItemState = {
    get state(): RepositoryItemState {
        return {
            "id": null,
            "node_id": null,
            "name": null,
            "full_name": null,
            "owner": defaultOwnerRepositoryState.state,
            "private": false,
            "html_url": null,
            "description": null,
            "fork": false,
            "url": null,
            "forks_url": null,
            "keys_url": null,
            "collaborators_url": null,
            "teams_url": null,
            "hooks_url": null,
            "issue_events_url": null,
            "events_url": null,
            "assignees_url": null,
            "branches_url": null,
            "tags_url": null,
            "blobs_url": null,
            "git_tags_url": null,
            "git_refs_url": null,
            "trees_url": null,
            "statuses_url": null,
            "languages_url": null,
            "stargazers_url": null,
            "contributors_url": null,
            "subscribers_url": null,
            "subscription_url": null,
            "commits_url": null,
            "git_commits_url": null,
            "comments_url": null,
            "issue_comment_url": null,
            "contents_url": null,
            "compare_url": null,
            "merges_url": null,
            "archive_url": null,
            "downloads_url": null,
            "issues_url": null,
            "pulls_url": null,
            "milestones_url": null,
            "notifications_url": null,
            "labels_url": null,
            "releases_url": null,
            "deployments_url": null,
            "created_at": null,
            "updated_at": null,
            "pushed_at": null,
            "git_url": null,
            "ssh_url": null,
            "clone_url": null,
            "svn_url": null,
            "homepage": null,
            "size": null,
            "stargazers_count": null,
            "watchers_count": null,
            "language": null,
            "has_issues": false,
            "has_projects": false,
            "has_downloads": false,
            "has_wiki": false,
            "has_pages": false,
            "forks_count": null,
            "mirror_url": null,
            "archived": false,
            "open_issues_count": null,
            "license": defaultLicenseRepositoryState.state,
            "forks": null,
            "open_issues": null,
            "watchers": null,
            "default_branch": null,
            "score": null,
        };
    }
};

const defaultDataRepositoryState = {
    get state(): DataRepositoryState {
        return {
            "total_count": 0,
            "incomplete_results": false,
            "items": [defaultRepositoryItemState.state]
        }
    }
}

const defaultSearchConditionsRepositoryState = {
    get state(): SearchConditionsRepositoryState {
        return {
            querySearch: null,
            stars: null,
            language: null,
            licence: null,
        };
    }
};

const defaultState = {
    get state(): RepositoriesState {
        return {
            data: defaultDataRepositoryState.state,
            isPending: false,
            searchConditions: defaultSearchConditionsRepositoryState.state,
        }
    }
};

export const initialState = {
    get state(): RepositoriesState {
        return defaultState.state;
    }
};
