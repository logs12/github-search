import React from 'react';
import injectSheet from 'react-jss';

import SearchWidget from '../../components/SearchWidget';
import RepositoryList from '../../components/RepositoryList';

const styles = {
  homePage: {
    textAlign: 'center',
  },
  
  homePageHeader: {
    backgroundColor: '#222',
    height: '50px',
    padding: '20px',
    color: 'white',
  },
  
  homePageTitle: {
    fontSize: '1.5em',
  },
  
  homePageIntro: {
    fontSize: 'large',
  }
};

const HomePage = injectSheet(styles)(({ classes }) => (
      <div className={classes.homePage}>
        <header className={classes.homePageHeader}>
          <h1 className={classes.homePageTitle}>Welcome to Github search</h1>
        </header>
        <div className={classes.homePageIntro}>

            <SearchWidget />
            <RepositoryList />
        </div>
      </div>
));

export default HomePage;
