import {
    PERFORM_CHANGE_REPOSITORIES_LIST,
    PERFORM_PENDING_ENABLE_REPOSITORIES_LIST,
    PERFORM_PENDING_DISABLE_REPOSITORIES_LIST,
    PERFORM_CHANGE_CONDITION_SEARCH,
} from '../actions/RepositoryAction';

import * as RepositoryModel from '../models/RepositoryModel';


function changeRepositoryList(state: RepositoryModel.DataRepositoryState, action) {
    return {
        ...state,
        ...action.payload,
    }
}

function pendingEnableRepositoryList(state: RepositoryModel.RepositoriesState, action) {
    return {
        ...state,
        ...{ isPending: action.payload },
    }
}

function pendingDisableRepositoryList(state: RepositoryModel.RepositoriesState, action) {
    return {
        ...state,
        ...{ isPending: action.payload },
    }
}

function performChangeConditionSearch(state: RepositoryModel.SearchConditionsRepositoryState, action) {
    return {
        ...state,
        ...{ [action.payload.typeField]: action.payload.value },
    }
}

export function repositoriesReducer(state: RepositoryModel.RepositoriesState = RepositoryModel.initialState.state, action) {
    switch (action.type) {
        case PERFORM_CHANGE_REPOSITORIES_LIST:
            return {
                ...state,
                ...{ data: changeRepositoryList(state.data, action) }
            };
        case PERFORM_PENDING_ENABLE_REPOSITORIES_LIST:
            return {
                ...state,
                ...pendingEnableRepositoryList(state, action)
            };
        case PERFORM_PENDING_DISABLE_REPOSITORIES_LIST:
            return {
                ...state,
                ...pendingDisableRepositoryList(state, action)
            };
        case PERFORM_CHANGE_CONDITION_SEARCH:
            return {
                ...state,
                ...{ searchConditions: performChangeConditionSearch(state.searchConditions, action) }
            };
        default:
            return state;
    }
}
