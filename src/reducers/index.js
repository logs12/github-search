import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';

import { repositoriesReducer } from './RepositoriesReducer';

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    combineReducers({
        repositories: repositoriesReducer,
    }),
    composeEnhancers(
        applyMiddleware(thunkMiddleware),
    )
);

export default store;
