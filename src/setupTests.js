import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import rmwcTestPolyfill from 'rmwc/Base/testPolyfill';

configure({ adapter: new Adapter() });

rmwcTestPolyfill();