import { api } from '../utils/api';
import apiPaths from '../utils/paths';
import * as RepositoryModel from '../models/RepositoryModel';
import * as RepositoryAction from '../actions/RepositoryAction';

export const performChangeConditionSearch = (
    value: any,
    typeField: RepositoryModel.TypeCondition
) => (
    dispatch: Function,
    getState: () => {}
): void => {
    dispatch(RepositoryAction.performChangeConditionSearch(value, typeField));
};


export const performSubmitSearch = (conditions: RepositoryModel.SearchConditionsRepositoryState) => (
    dispatch: Function,
    getState: () => {}
): void => {

    dispatch(RepositoryAction.performEnablePendingRepository());

    let querySearch: string = ( conditions.querySearch !== null) ? conditions.querySearch : '';

    if ( conditions.language !== null) {
        querySearch += `+language:${conditions.language}`
    }

    if ( conditions.stars !== null) {
        querySearch += `+stars:${conditions.stars}`
    }

    if ( conditions.licence !== null) {
        querySearch += `+licence:${conditions.licence}`
    }

    api()
        .get(apiPaths.searchRepositories.replace(':querySearch', querySearch))
        .then(
            response => {
                dispatch(RepositoryAction.performDisablePendingRepository());
                dispatch(RepositoryAction.performChangeRepository(response.data));
            },
            err => {
                dispatch(RepositoryAction.performDisablePendingRepository());
            }
        );
};

export const performGetDefaultRepositoryList = () => (
    dispatch: Function,
    getState: () => {}
): void => {


    const searchConditionsState: RepositoryModel.SearchConditionsRepositoryState = getState().repositories.searchConditions;

    if (
        searchConditionsState.querySearch === null &&
        searchConditionsState.stars === null &&
        searchConditionsState.language === null &&
        searchConditionsState.licence === null
    ) {
        dispatch(RepositoryAction.performEnablePendingRepository());
        api()
            .get(apiPaths.searchTopRepositories)
            .then(
                response => {
                    dispatch(RepositoryAction.performDisablePendingRepository());
                    dispatch(RepositoryAction.performChangeRepository(response.data));
                },
                err => {
                    dispatch(RepositoryAction.performDisablePendingRepository());
                }
            );
    }

};
