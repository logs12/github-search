import axios from 'axios';

import projectConfig from '../project.config.json';

export const api = (headers = {}) =>
    axios.create({
        ...projectConfig.API,
        headers: {
            ...headers
        }
    });

export default api;
