export default {
    searchRepositories: 'search/repositories?q=:querySearch:assembly&sort=:sort&order=desc',
    searchTopRepositories: 'search/repositories?q=stars:>=500&order=desc&per_page=10',
};
